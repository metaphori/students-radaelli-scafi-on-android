scalaVersion := "2.11.8"

enablePlugins(AndroidApp)
useSupportVectors

versionCode := Some(1)
version := "0.1-SNAPSHOT"

instrumentTestRunner :=
  "android.support.test.runner.AndroidJUnitRunner"

platformTarget in Android:= "android-23"

//minSdkVersion in Android := "19"

javacOptions in Compile ++= "-source" :: "1.7" :: "-target" :: "1.7" :: Nil

libraryDependencies ++=
  "com.android.support" % "appcompat-v7" % "24.0.0" ::
  "com.android.support.test" % "runner" % "0.5" % "androidTest" ::
  "com.android.support.test.espresso" % "espresso-core" % "2.2.2" % "androidTest" ::
  "com.google.android.gms" % "play-services" % "9.8.0" ::
  "com.android.support" % "multidex" % "1.0.0" ::
Nil

// Activate proguard for Scala
proguardScala in Android := true

// Activate proguard for Android
useProguard in Android := true

// Set proguard options
proguardOptions in Android ++= Seq(
		"-ignorewarnings",
		"-keep class scala.Dynamic")

dexMulti in Android := true
//dexMaxHeap in Android := "4g"

// AKKA
// Constants
val akkaVersion = "2.3.7" //NOTE: Akka 2.4.0 REQUIRES Java 8!
// Managed dependencies
val akkaActor  = "com.typesafe.akka" % "akka-actor_2.11"  % akkaVersion
val akkaRemote = "com.typesafe.akka" % "akka-remote_2.11" % akkaVersion

libraryDependencies ++= Seq(akkaActor, akkaRemote)

// ProGuard rules for Akka
proguardOptions in Android ++= Seq(
		"-keep class akka.actor.Actor$class { *; }",
		"-keep class akka.actor.LightArrayRevolverScheduler { *; }",
		"-keep class akka.actor.LocalActorRefProvider { *; }",
		"-keep class akka.actor.CreatorFunctionConsumer { *; }",
		"-keep class akka.actor.TypedCreatorFunctionConsumer { *; }",
		"-keep class akka.dispatch.BoundedDequeBasedMessageQueueSemantics { *; }",
		"-keep class akka.dispatch.UnboundedMessageQueueSemantics { *; }",
		"-keep class akka.dispatch.UnboundedDequeBasedMessageQueueSemantics { *; }",
		"-keep class akka.dispatch.DequeBasedMessageQueueSemantics { *; }",
		"-keep class akka.dispatch.MultipleConsumerSemantics { *; }",
		"-keep class akka.actor.LocalActorRefProvider$Guardian { *; }",
		"-keep class akka.actor.LocalActorRefProvider$SystemGuardian { *; }",
		"-keep class akka.dispatch.UnboundedMailbox { *; }",
		"-keep class akka.actor.DefaultSupervisorStrategy { *; }",
		"-keep class macroid.akka.AkkaAndroidLogger { *; }",
		"-keep class akka.event.Logging$LogExt { *; }"
)

//SCAFI
val scafi_core  = "it.unibo.apice.scafiteam" % "scafi-core_2.11"  % "0.1.0"
val scafi_simulator  = "it.unibo.apice.scafiteam" % "scafi-simulator_2.11"  % "0.1.0"
val scafi_platform = "it.unibo.apice.scafiteam" % "scafi-distributed_2.11"  % "0.1.0"

libraryDependencies ++= Seq(scafi_core, scafi_simulator, scafi_platform)

packagingOptions in Android := PackagingOptions(excludes = Seq("reference.conf", "META-INF/NOTICE.txt", "META-INF/LICENSE.txt"))
