package it.unibo

import android.content.{DialogInterface, Intent}
import android.os.Bundle
import android.provider.Settings
import android.support.v7.app.{AlertDialog, AppCompatActivity}

class AlertDialogActivity extends AppCompatActivity {

		override def onCreate(savedInstanceState: Bundle) {
				super.onCreate(savedInstanceState)

				val builder = new AlertDialog.Builder(this)
				builder
						.setCancelable(false)
					  .setMessage("Enable data connection to continue...")
						.setPositiveButton("Setting", new DialogInterface.OnClickListener() {
								def onClick(dialog: DialogInterface, id: Int): Unit = {
										startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS))
										finish
								}
						})
				builder.show
		}
}