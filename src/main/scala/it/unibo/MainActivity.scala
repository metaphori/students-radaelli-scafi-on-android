package it.unibo

import akka.actor.{ActorSystem, Props}
import android.content._
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.typesafe.config.ConfigFactory

/*class MainActivity extends AppCompatActivity {
    // allows accessing `.value` on TR.resource.constants
    implicit val context = this

    override def onCreate(savedInstanceState: Bundle): Unit = {
        super.onCreate(savedInstanceState)
        // type ascription is required due to SCL-10491
        val vh: TypedViewHolder.main = TypedViewHolder.setContentView(this, TR.layout.main)
        vh.text.setText(s"Hello world, from ${TR.string.app_name.value}")
        vh.image.getDrawable match {
          case a: Animatable => a.start()
          case _ => // not animatable
        }
    }
}*/

class MainActivity extends AppCompatActivity with TypedFindView {

		var receiver: NetworkReceiver = null

		val custom = ConfigFactory.parseString(ReferenceConf.toString)
		val system = ActorSystem("mySystem", ConfigFactory.load(custom))
		val actor = system.actorOf(Props(classOf[PingActor]))

		override def onCreate(savedInstanceState: Bundle) = {
				super.onCreate(savedInstanceState)
				setContentView(R.layout.main2)
				//val text = findViewById(R.id.text).asInstanceOf[TextView]
				val text = findView(TR.text)
				text.setText(text.getText + " on Android!")
				//val buttonMap = findViewById(R.id.buttonMap).asInstanceOf[Button]
				val buttonMap = findView(TR.buttonMap)

				buttonMap.setOnClickListener(new View.OnClickListener {

						override def onClick(v: View): Unit = {
								Toast.makeText(getApplicationContext(), "I'm going to map activity", Toast.LENGTH_SHORT).show

								actor ! Ping

								val intent = new Intent(MainActivity.this, classOf[MapActivity])
								startActivity(intent)
						}
				})

				val buttonAkka = findView(TR.buttonAkka)

				buttonAkka.setOnClickListener(new View.OnClickListener {

						override def onClick(v: View): Unit = {
								Toast.makeText(getApplicationContext(), "Send ping", Toast.LENGTH_SHORT).show
								actor ! Ping
						}
				})

				val buttonScafi = findView(TR.buttonScafi)

				buttonScafi.setOnClickListener(new View.OnClickListener {

						override def onClick(v: View): Unit = {
								Toast.makeText(getApplicationContext(), "Launch Scafi", Toast.LENGTH_SHORT).show
								val intent = new Intent(MainActivity.this, classOf[ScafiActivity])
								startActivity(intent)
						}
				})
		}

		override def onPause: Unit = {
				unregisterReceiver(receiver)
				super.onPause
		}

		override def onResume: Unit = {
				receiver = new NetworkReceiver
				registerReceiver(receiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
				super.onResume
		}
}