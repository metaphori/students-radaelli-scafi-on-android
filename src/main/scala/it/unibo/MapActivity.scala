package it.unibo

import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import com.google.android.gms.maps.model.{CameraPosition, LatLng, MarkerOptions}
import com.google.android.gms.maps.{CameraUpdateFactory, GoogleMap, OnMapReadyCallback, SupportMapFragment}

class MapActivity extends FragmentActivity with OnMapReadyCallback {

		var receiver: NetworkReceiver = null

		override def onCreate(savedInstanceState: Bundle) = {
				super.onCreate(savedInstanceState)
				setContentView(R.layout.map_layout)

				val mapFragment = getSupportFragmentManager.findFragmentById(R.id.map).asInstanceOf[SupportMapFragment]
				mapFragment.getMapAsync(this)

		}

		override def onMapReady(googleMap: GoogleMap) {
				val mMap = googleMap

				val cesena = new LatLng(44.1390945, 12.2429281)
				mMap.addMarker(new MarkerOptions().position(cesena).title("Cesena is here!"))
				val cameraPosition = new CameraPosition.Builder().target(cesena).zoom(15).build

				mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

				mMap.setMyLocationEnabled(true)
		}

		override def onPause: Unit = {
				unregisterReceiver(receiver)
				super.onPause
		}

		override def onResume: Unit = {
				receiver = new NetworkReceiver
				registerReceiver(receiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
				super.onResume
		}
}