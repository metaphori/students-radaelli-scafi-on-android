package it.unibo

import android.content._
import android.net.ConnectivityManager
import android.util.Log

class NetworkReceiver extends BroadcastReceiver {

		override def onReceive(context: Context, intent: Intent) {

				val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE).asInstanceOf[ConnectivityManager]
				val activeNetwork = connectivityManager.getActiveNetworkInfo

				if(activeNetwork != null && activeNetwork.isConnectedOrConnecting) {
						Log.i("Network available", "yes")
						//Toast.makeText(context, "Network available on", Toast.LENGTH_SHORT).show
				} else {
						Log.i("Network available", "no")
						//Toast.makeText(context, "Network available off", Toast.LENGTH_SHORT).show

						val intent = new Intent(context, classOf[AlertDialogActivity])
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
						context.startActivity(intent)
				}
		}
}
