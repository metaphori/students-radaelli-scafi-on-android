package it.unibo

import it.unibo.scafi.incarnations.BasicSimulationIncarnation._
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class ScafiActivity extends AppCompatActivity with TypedFindView {

		val ctx = new ContextImpl(selfId = 1, exports = Map(), localSensor = Map(), nbrSensor = Map())

		override def onCreate(savedInstanceState: Bundle) = {
				super.onCreate(savedInstanceState)
				setContentView(R.layout.scafi_layout)

				val response = findView(TR.response)

				MyProgram.round(ctx)

				response.setText(MyProgram.round(ctx).toString()) // output atteso: Map(P:/ -> 1) ERRORE!!


				/*val net = simulatorFactory.gridLike(
						n = 10,
						m = 10,
						stepx = 1,
						stepy = 1,
						eps = 0.0,
						rng = 1.1)

				net.addSensor(name = "source", value = false)
				net.chgSensorValue(name = "source", ids = Set(3), value = true)

				var v = java.lang.System.currentTimeMillis()

				net.executeMany(
						node = MyProgram,
						size = 100000,
						action = (n,i) => {
								if (i % 1000 == 0) {
										println(net)
										val newv = java.lang.System.currentTimeMillis()
										println(newv-v)
										println(net.context(4))
										v=newv
								}
						}
				)*/
		}
}
